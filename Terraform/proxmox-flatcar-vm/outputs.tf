output "config_rendered" {
    value = data.ct_config.flatcar_config.rendered
    description = "Resulting config for the VM"
    sensitive = true
}

output "vm_path" {
    value = proxmox_virtual_environment_vm.flatcar_vm.vm_id
    description = "Full ID of the created VM"
}

output "vm_uuid" {
    value = random_uuid.vm_instance.result
    description = "Random UUID that will always and only change when the VM is recreated"
}
