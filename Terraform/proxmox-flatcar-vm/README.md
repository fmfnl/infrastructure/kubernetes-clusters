# Terraform Module for a Flatcar VM in Proxmox

This Terraform Module contains all stuff to automatically deploy a virtual machine to Proxmox runnign Flatcar OS, set up with FMF SSH integration. It will be authenticated to Vault.

## Inputs

General module settings:

- `provisioning_config`: An object containing the provisioning settings for the VM, in particular the hostname of the Proxmox server and SSH credentials to access it.
- `replace_on`: String value that, when changed, will trigger a VM recreation.

VM behaviour setings:

- `id`: The numeric ID of the VM in Proxmox
- `name`: The name of the VM
- `description`: Description for the Proxmox UI (default: `""`)
- `node`: The particular node in the Proxmox cluster to deploy the VM to
- `autostart`: Whether to autostart the VM after host boot, specifying the boot order and timeout
- `approle_role`: The Vault AppRole role to use for obtaining credentials for the VM (default: `host-ssh`)

VM configuration:

- `template`: Name of the template VM to clone; this disk image will be used
- `core_count`: Number of cores
- `memory`: Memory settings for the VM
- `networks`: List of networks to connect to (bridge name and CIDR settings)
- `drives`: List of drives to attach

## Outputs

- `config_rendered`: For investigation, the full rendered configuration that is applied to the VM.
- `vm_path`: Full Proxmox representation of the ID of the new VM.
- `vm_uuid`: Random UUID that will always and only change when the VM is recreated.
