# Adding AppRole resources for this VM to automate Vault authentication
data "vault_approle_auth_backend_role_id" "approle_role" {
    backend = "approle"
    role_name = var.approle_role
}
