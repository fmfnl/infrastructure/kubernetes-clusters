variable "provisioning_config" {
    type = object({
        ssh = object({
            hostname = string
            username = string
            private_key_file = string
            certificate_file = optional(string)
        })
        hostname = optional(string)
        additional_configs = list(string)
    })
    description = "The provisioning settings for the VM"
    sensitive = true
}

variable "autostart" {
    type = object({
        order = optional(number)
        timeout = optional(number)
    })
    description = "Whether to autostart the VM after host boot"
    default = null
}

variable "id" {
    type = number
    description = "Numeric ID of the VM"
    default = 0
}

variable "name" {
    type = string
    description = "Name of the VM"
}

variable "description" {
    type = string
    description = "Description of the VM in the Proxmox UI"
    default = ""
}

variable "node" {
    type = string
    description = "The Proxmox node to deploy the VM to"
}

variable "template" {
    type = number
    description = "ID of the template VM to clone, that contains the disk image"
}

variable "memory" {
    type = object({
        maximum = number
        minimum = optional(number)
    })
    description = "The memory settings for the VM"
}

variable "core_count" {
    type = number
    description = "Number of cores for the VM"
}

variable "networks" {
    type = list(object({
        bridge_name = string

        address = string
        range = string
        gateway = optional(string)
        dns = optional(string)
        extra = optional(string)
    }))
    description = "List of network devices"
}

variable "drives" {
    type = list(object({
        size = number
        backing_storage = string
        interface = string
    }))
    description = "List of drive configurations"
}

variable "approle_role" {
    type = string
    sensitive = true
    default = "host-ssh"
    description = "Vault AppRole role to use for obtaining AppRole credentials for this VM"
}

variable "replace_on" {
    type = string
    sensitive = false
    default = "neverchange"
    description = "When this value changes, the VM and configuration will be recreated and reprovisioned"
}
