locals {
    private_key = file(var.provisioning_config.ssh.private_key_file)
    certificate = fileexists(var.provisioning_config.ssh.certificate_file) ? file(var.provisioning_config.ssh.certificate_file) : null

    config_file = "/var/lib/vz/snippets/fmf-machines/${var.id}-${var.name}.config.ign"
    secret_file = "/var/lib/vz/snippets/fmf-machines/${var.id}-${var.name}.secret"
    secret_filename = "${var.id}-${var.name}.secret"
    vm_hostname = var.provisioning_config.hostname == null ? var.name : var.provisioning_config.hostname
}
