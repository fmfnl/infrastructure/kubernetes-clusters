# Obtain a token to register a Gitlab Cluster Agent
# Ideally, the token is recycled whenever the VM is recreated, but this is not
# possible at the moment due to cyclicity issues.

resource "gitlab_cluster_agent" "rancher-server-agent" {
    project = data.gitlab_project.current_project.id
    name = "rancher-server"
}

resource "gitlab_cluster_agent_token" "rancher-server-agent-token" {
    project = data.gitlab_project.current_project.id
    agent_id = gitlab_cluster_agent.rancher-server-agent.agent_id
    name = "rancher-server-token"
    description = "Token for the GitLab Runner on the Rancher Server cluster"

    lifecycle {
        replace_triggered_by = [
            gitlab_cluster_agent.rancher-server-agent,
            random_uuid.project-seed
        ]
    }
}

# We will also store this token in the Vault CI KV2 storage, so it can later
# be used by the helmfile pipelines for keeping the Cluster Agent up-to-date.
resource "vault_kv_secret_v2" "rancher-server-agent-token" {
    mount = "secrets-ci"
    name = "gitlab/agent-tokens/management"
    data_json = jsonencode({
        token = gitlab_cluster_agent_token.rancher-server-agent-token.token
    })
}

# In addition, we store the Runners token in the Vault CI KV2 storage.
resource "vault_kv_secret_v2" "rancher-server-runner-token" {
    mount = "secrets-ci"
    name = "gitlab/runner-tokens/management"
    data_json = jsonencode({
        token = data.gitlab_group.fmf_group.runners_token
    })
}
