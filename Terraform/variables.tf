variable "proxmox_server" {
    type = string
    description = "Proxmox server url"
}

variable "proxmox_insecure" {
    type = bool
    default = false
    description = "Whether to allow insecure connections (invalid certificate)"
}

# We have to use username/password credentials instead of an API token
# since API tokens cannot yet have permission to set "args" of a VM in Proxmox
# https://bugzilla.proxmox.com/show_bug.cgi?id=2582
variable "proxmox_credentials" {
    type = object({
      username = string,
      password = string
    })
    description = "Credentials to log in to the Proxmox server"
    sensitive = true
}

#variable "proxmox_api_token" {
#   type = string
#    description = "Proxmox API token ID"
#    sensitive = true
#}

#variable "proxmox_api_secret" {
#    type = string
#    sensitive = true
#    description = "Proxmox API token secret"
#}

variable "proxmox_ssh_config" {
    type = object({
        hostname = optional(string)
        username = string
        private_key_file = string
        certificate_file = string
    })
    description = "SSH config of the Proxmox server to deploy to"
    sensitive = true
}

variable "cloudflare_api_token" {
    type = string
    description = "API Token for Cloudflare"
    sensitive = true
}

variable "rancher_admin_password" {
    type = string
    description = "Admin password after bootstrapping Rancher Server"
    sensitive = true
}

variable "ldap_config" {
    type = object({
        hostname = string
        username = string
        password = string
        testuser = string
        testpass = string
    })

    description = "LDAP Configuration to use for the Rancher server"
    sensitive = true
}
