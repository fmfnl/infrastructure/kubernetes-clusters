# Generate random password, this won't be regenerated until tainted manually
resource "random_password" "rancher-server-initial-password" {
    length = 64
    special = false # Can generate #-symbols that will cause issues

    keepers = {
        seed = random_uuid.project-seed.result
    }
}

# Delay a bit until the server is done provisioning, about 5 minutes
resource "time_sleep" "rancher-server-flatcar-boot-wait" {
    depends_on = [
        module.rancher-server-flatcar
    ]

    create_duration = "5m"

    # Always recreate when the VM is redeployed; this depends on a randomly-
    # generated UUID that will only and always be renewed when the VM is
    # recreated. So this should be fine.
    triggers = {
        config = module.rancher-server-flatcar.vm_uuid
    }
}

# Rancher bootstrapper
provider "rancher2" {
    alias = "bootstrapper"

    bootstrap = true
    insecure = true
    api_url = "https://management.internal.fmf.nl"
}

resource "rancher2_bootstrap" "rancher-server-bootstrap" {
    provider = rancher2.bootstrapper

    depends_on = [
        time_sleep.rancher-server-flatcar-boot-wait
    ]

    lifecycle {
        replace_triggered_by = [
            time_sleep.rancher-server-flatcar-boot-wait
        ]
    }

    initial_password = random_password.rancher-server-initial-password.result
    password = var.rancher_admin_password

    ui_default_landing = "ember"
}
