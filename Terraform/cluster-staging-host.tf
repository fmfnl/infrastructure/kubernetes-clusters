locals {
    staging-rancher-flatcar-primary-ip = "129.125.36.7"
}

# Add cluster node
module "staging-rancher-flatcar" {
    source = "./proxmox-flatcar-vm"

    replace_on = rancher2_cluster_v2.cluster-staging-rancher.id
    depends_on = [
        cloudflare_record.staging-rancher-public
    ]

    provisioning_config = {
        ssh = merge(var.proxmox_ssh_config, { hostname = "elysium.fmf.nl" })
        additional_configs = [
            # Configure filesystem watch limit
            file("${path.module}/resources/unit-sysctl-watches.yaml"),

            # Deploy RKE2 with management integration
            templatefile("${path.module}/resources/unit-rke2-install-register.yaml", {
                command = "${rancher2_cluster_v2.cluster-staging-rancher.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker --node-name staging-flatcar"
            }),

            # Install GitLab Cluster Agent within cluster
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.staging-agent-token.token
                    version = local.staging_gitlab_agent_version
                })
            })
        ]
    }

    id = 412
    name = "staging-rancher"
    description = "Staging cluster node running flatcar OS"
    node = "elysium"
    template = 502
    memory = {
        maximum = 16384
        minimum = 12288
    }
    core_count = 4

    networks = [
        {
            bridge_name = "public"
            address = "${local.staging-rancher-flatcar-primary-ip}"
            range = "/32"
            dns = "1.1.1.1"
            extra = <<-EOT
                [Route]
                Gateway=10.0.0.1
                GatewayOnLink=yes
            EOT
        },
        {
            bridge_name = "intshare"
            address = "10.9.1.114"
            range = "/24"
        },
        {
            bridge_name = "intstor"
            address = "10.9.2.114"
            range = "/24"
        }
    ]

    drives = [{
        size = 64
        backing_storage = "vm_boot"
        interface = "scsi0"
    }]

    autostart = {
        timeout = 120
    }
}

# Add DNS registration
resource "cloudflare_record" "staging-rancher-public" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.staging"
    type = "A"
    value = local.staging-rancher-flatcar-primary-ip
}

# Write the DNS configuration to the Vault Kubernetes auth
resource "vault_kubernetes_auth_backend_config" "staging-rancher-config" {
    backend = "kubernetes-staging"
    kubernetes_host = cloudflare_record.staging-rancher-public.hostname
    kubernetes_ca_cert = "will be determined later but we have to enter it already"
}
