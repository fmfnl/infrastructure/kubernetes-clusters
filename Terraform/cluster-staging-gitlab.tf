# Obtain a token to register a GitLab Cluster Agent
# Ideally, the token is recycled whenever the VM is recreated, but
# this is currently not possible due to cyclicity issues.

resource "gitlab_cluster_agent" "staging-agent" {
    project = data.gitlab_project.current_project.id
    name = "staging"
}

resource "gitlab_cluster_agent_token" "staging-agent-token" {
    project = data.gitlab_project.current_project.id
    agent_id = gitlab_cluster_agent.staging-agent.agent_id
    name = "staging-token"
    description = "Token for the GitLab Runner on the Staging cluster"

    lifecycle {
        replace_triggered_by = [
            gitlab_cluster_agent.staging-agent,
            terraform_data.cluster-staging-instance
        ]
    }
}

# We will also store this token in the Vault CI KV2 storage, so it can later
# be used by the helmfile pipelines for keeping the Cluster Agent up-to-date
resource "vault_kv_secret_v2" "staging-agent-token" {
    mount = "secrets-ci"
    name = "gitlab/agent-tokens/staging"
    data_json = jsonencode({
        token = gitlab_cluster_agent_token.staging-agent-token.token
    })
}
