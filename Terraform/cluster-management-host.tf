locals {
    rancher-server-flatcar-primary-ip = "10.9.1.103"
}

module "rancher-server-flatcar" {
    source = "./proxmox-flatcar-vm"
    
    replace_on = random_uuid.project-seed.result
    depends_on = [
        cloudflare_record.rancher-server-internal
    ]

    provisioning_config = {
        ssh = merge(var.proxmox_ssh_config, { hostname = "delphi.fmf.nl"})
        additional_configs = [
            # Install RKE2
            templatefile("${path.module}/resources/unit-rke2-install.yaml", {
                version = local.management_rke2_version
            }),

            # Modify nginx ingress within RKE2
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "rke2-ingress-config.yaml"
                content = file("${path.module}/resources/k8s-crd-rke2-ingress-config.yaml")
            }),

            # Install Cert Manager within RKE2
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "cert-manager.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-cert-manager.yaml", {
                    version = local.management_cert_manager_version
                })
            }),

            # Install Rancher Server within RKE2
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "rancher.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-rancher.yaml", {
                    initial_password = random_password.rancher-server-initial-password.result
                    hostname = cloudflare_record.rancher-server-internal.hostname
                    version = local.rancher_version
                })
            }),

            # Install GitLab Cluster Agent within RKE2
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.rancher-server-agent-token.token
                    version = local.management_gitlab_agent_version
                })
            }),

            # Install GitLab Runner within RKE2
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "gitlab-runner.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-runner.yaml", {
                    token = data.gitlab_group.fmf_group.runners_token
                    version = local.management_gitlab_runner_version
                    url = "https://gitlab.com/"
                })
            })
        ]
    }

    id=917
    name = "rancher-server"
    description = "Flatcar OS VM running the Rancher Server"
    node = "delphi"
    template = 502
    memory = {
        maximum = 8192
        minimum = 6144
    }
    core_count = 2

    networks = [
        {
            bridge_name = "intshare"
            address = "${local.rancher-server-flatcar-primary-ip}"
            range = "/24"
            gateway = "10.9.1.1"
            dns = "1.1.1.1"
        }
    ]

    drives = [{
        size = 64
        backing_storage = "vm_boot"
        interface = "scsi0"
    }]

    autostart = {
        order = 3
    }
}

# Add DNS registration
resource "cloudflare_record" "rancher-server-internal" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "management.internal"
    type = "A"
    value = local.rancher-server-flatcar-primary-ip
}

# Write the DNS configuration to the Vault Kubernetes auth
resource "vault_kubernetes_auth_backend_config" "rancher-server-config" {
    backend = "kubernetes-management"
    kubernetes_host = cloudflare_record.rancher-server-internal.hostname
    kubernetes_ca_cert = "will be determined later but we have to enter it already"
}
